{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style='background-image: url(\"header.png\") ; padding: 0px ; background-size: cover ; border-radius: 5px ; height: 250px'>\n",
    "    <div style=\"float: right ; margin: 50px ; padding: 20px ; background: rgba(255 , 255 , 255 , 0.7) ; width: 50% ; height: 150px\">\n",
    "        <div style=\"position: relative ; top: 50% ; transform: translatey(-50%)\">\n",
    "            <div style=\"font-size: xx-large ; font-weight: 900 ; color: rgba(0 , 0 , 0 , 0.8) ; line-height: 100%\">Salvus</div>\n",
    "            <div style=\"font-size: large ; padding-top: 20px ; color: rgba(0 , 0 , 0 , 0.5)\">FWI workflows and near-source effects</div>\n",
    "        </div>\n",
    "    </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "Seismic full-waveform inversion (FWI) is a nonlinear tomographic method which attempts to iteratively construct 3-D models of subsurface geology. In contrast to approaches based on arrival times, full-waveform inversion is characterized by numerical solutions to the equations of motion and a measure of misfit based on waveform differences between observed and synthetic seismograms. The combination of these factors allows for improved tomographic resolution while negating the need to identify specific waveform phases in the data.\n",
    "\n",
    "While the mathematical foundations of FWI can be written down in a few lines, practical issues including data and workflow management often plague real-world applications. In this tutorial we will start from scratch and run a small-scale inversion to see how Salvus_Mesh_, Salvus_Compute_, Salvus_Flow_, and Salvus_Opt_ work together to mitigate some of the common problems facing realistic applications of FWI. This will be followed by a short addendum where, given our inverted model, we investigate the effect of fine-scale near-source structure on our synthetic waveforms.\n",
    "\n",
    "As a first step, let's import our tools."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python libraries.\n",
    "import os\n",
    "import copy\n",
    "import toml\n",
    "import shutil\n",
    "import numpy as np\n",
    "\n",
    "from pathlib import Path\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Salvus.\n",
    "import pyasdf            # ASDF library for visualizing seismograms.\n",
    "import salvus_mesh       # Salvus meshing toolbox.\n",
    "import salvus_flow.api   # Salvus workflow toolbox.\n",
    "\n",
    "# Model helper.\n",
    "import soil_builder.domain\n",
    "from soil_builder.inclusion import Quadrilateral, Triangle\n",
    "\n",
    "# FWI workflow helpers.\n",
    "from salvus_flow.workflow import Workflow\n",
    "from salvus_flow.workflow.helpers import (\n",
    "    get_misfit_and_adjoint_source_callback_function,\n",
    "    get_sources_and_receivers_callback_function\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model generation\n",
    "\n",
    "Since this is a synthetic study, we need to generate our starting and true models. We've put together a small package called `soil_builder` which can be used to generate some domains with inclusions. Currently we support both Triangular and Quadrilateral inclusions, but it is trival to add your own. Once just needs to add a new implementation of an `Inclusion` class, and provide the `is_inside(x)` method. The model below is set up similar to the models passed on in the provided Word document."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build our domain w/ inclusions.\n",
    "r1 = np.array([[2.3, 2.6], [4.0, 3.0], [4.0, 3.8], [2.3, 3.3]])\n",
    "r2 = np.array([[1.0, 1.0], [4.0, 1.4], [4.0, 2.2], [1.0, 1.7]])\n",
    "\n",
    "# Can add more inclusions if you wish.\n",
    "r3 = np.array([[1.5, 0.0], [2.4, 0.0], [2.7, 3.5], [1.9, 3.5]])\n",
    "rt0 = np.array([[2.3, 2.6], [2.3, 3.3], [1.0 ,2.95]])\n",
    "rt1 = np.array([[0.0, 0.0], [0.5, 0.0], [0.5, 0.5]])\n",
    "\n",
    "# Velocities (these can be different per inclusion).\n",
    "p_soil = {\"RHO\": 1500.0, \"VP\": 950.0, \"VS\": 600.0}\n",
    "p_rock = {\"RHO\": 1900.0, \"VP\": 2600.0, \"VS\": 1800.0}\n",
    "p_starting = {\"RHO\": 1700.0, \"VP\": 1775.0, \"VS\": 1200.0}\n",
    "\n",
    "# Initialize our models.\n",
    "true_model = soil_builder.domain.Domain(4, 4, p_rock)\n",
    "starting_model = soil_builder.domain.Domain(4, 4, p_starting)\n",
    "\n",
    "# Attach the desired inclusions.\n",
    "true_model.attach_inclusion(Quadrilateral(r1, p_soil))\n",
    "true_model.attach_inclusion(Quadrilateral(r2, p_soil))\n",
    "\n",
    "# Extra inclusions.\n",
    "# true_model.attach_inclusion(Triangle(rt0, p_soil))\n",
    "# true_model.attach_inclusion(Triangle(rt1, p_soil))\n",
    "# true_model.attach_inclusion(Quadrilateral(r3, p_soil))\n",
    "\n",
    "# Quick plot to see what we're doing.\n",
    "f, ax = true_model.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We often know a-priori that our models cannot exceed a certain set of bounds on each parameter. We can optionally include this information in the inversion, and Salvus_Opt_ will ensure that the model updates remain within the 'feasible set'. It's good to try the inversion with and without these constraints to see their effect. It can be quite profound! This sheds some interesting light on the effect of 'cross-talk' between parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Add upper and lower bound constraints.\n",
    "starting_model.add_constraints(\"lower\", {\"VP\": p_soil[\"VP\"], \"VS\": p_soil[\"VS\"], \"RHO\": p_soil[\"RHO\"]})\n",
    "starting_model.add_constraints(\"upper\", {\"VP\": p_rock[\"VP\"], \"VS\": p_rock[\"VS\"], \"RHO\": p_rock[\"RHO\"]})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now can just use `soil_builder` to generate a mesh appropriate for a Salvus_Compute_ simulation. We just need to specify a few parameters describing the accuracy of the wave simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Mesh will be accurate up to max_frequency.\n",
    "max_frequency, source_frequency = 2500., 500.\n",
    "\n",
    "# Our starting model will be accurate to the minimum expected velocity.\n",
    "starting_mesh = starting_model.mesh(\n",
    "    max_frequency=max_frequency, \n",
    "    min_velocity=p_soil[\"VS\"], \n",
    "    num_absorbing_layers=0)\n",
    "\n",
    "# Our true model is meshed for the minimum velocity in each inclusion.\n",
    "true_mesh = true_model.mesh(\n",
    "    max_frequency=max_frequency, \n",
    "    min_velocity=\"inclusion\", \n",
    "    smoothing_length=0.15, \n",
    "    num_absorbing_layers=0)\n",
    "\n",
    "# Write the starting model.\n",
    "starting_mesh.write_exodus(\"starting_model.e\", overwrite=True)\n",
    "\n",
    "# Visualize our true model.\n",
    "true_mesh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Looking very cool! Now let's continue and generate some \"field\" data to use for a full-waveform inversion."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generating field data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next step is to actually define our survey. Since this is a synthetic example, we'll use our omnipotence to acheive better coverage than we'd ever get in a realistic situation. Below we're going to set up some source and receivers in our domain. At this point, we are making the assumption that our simulations will be driven by Salvus_Flow_. This is why we're storing the the sources and receivers into the specific dictionary formats that you see. For more information on what formats Salvus_Flow_ expects, check out the documentation on our website."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up receiver locations.\n",
    "num_rec = 16\n",
    "rec_x = np.ones(num_rec) * 3.5\n",
    "rec_y = np.linspace(0.5, 3.5, num_rec)\n",
    "\n",
    "# Set up source locations.\n",
    "num_src = 4\n",
    "src_x = np.ones(num_src) * 0.5\n",
    "src_y = np.linspace(0.5, 3.5, num_src)\n",
    "\n",
    "# Generate receivers.\n",
    "receivers = [{\n",
    "    \"network-code\": \"XX\",\n",
    "    \"station-code\": f\"{_i:03d}\",\n",
    "    \"medium\": \"solid\",\n",
    "    \"location\": [float(x), float(y)]}\n",
    "    for _i, (x, y) in enumerate(zip(rec_x, rec_y))]\n",
    "\n",
    "# Generate simulations.\n",
    "simulations = {\n",
    "    f\"simulation_{_i:03d}\" : {\n",
    "        \"source\" : [{\n",
    "            \"name\": f\"shot_{_i}\",\n",
    "            \"location\": [float(x), float(y)],\n",
    "            \"spatial_type\": \"vector\",\n",
    "            \"temporal_type\": \"ricker\",\n",
    "            \"center_frequency\": source_frequency,\n",
    "            \"scale\": [1e9, 0.0]}],\n",
    "        \"receiver\": copy.deepcopy(receivers)}\n",
    "    for _i, (x, y) in enumerate(zip(src_x, src_y))}\n",
    "\n",
    "# Attach sources and receivers to the mesh object for visualization.\n",
    "true_mesh._meta = {\"simulations\": simulations}\n",
    "\n",
    "# Visualize.\n",
    "true_mesh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we're just about ready to go and generate our \"observed\" seismic data. Let's review what we'll use for this:\n",
    "\n",
    "* `true_model`, which is our ground-truth simulation mesh with the Gaussian blob attached\n",
    "* `receivers`, which is a list of objects defining our seismic stations, and\n",
    "* `simulations`, which is a dictionary describing information about the four simulations we've defined\n",
    "\n",
    "The final thing to do is then to finish up our input file for Salvus_Compute_, and then let Salvus_Flow_ handle the workflow for us. Salvus_Flow_ expects the Salvus_Compute_ input files to be organized in a Python dictionary. Below is a small function which shows how you can hierarchically build the different sections of an input file. Note that, before running, all the input files are validated against a `JSON` schema both within Salvus_Flow_ itself, and also within Salvus_Compute_. This ensures that most common errors can be caught before a big job is even spun up."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_master_input(model_file: str):\n",
    "    \"\"\"\n",
    "    Generate a skelton input file which will use a certain model file.\n",
    "    :model_file: Model file to use in the simulation.\n",
    "    \"\"\"\n",
    "\n",
    "    master_file = {}\n",
    "    \n",
    "    # Provide information on the specific mesh and the model.\n",
    "    master_file.update({\"domain\": {\n",
    "            \"dimension\": 2,\n",
    "            \"polynomial-order\": 4,\n",
    "            \"mesh\": {\n",
    "                \"filename\": model_file,\n",
    "                \"format\": \"exodus\"\n",
    "            },\n",
    "            \"model\": {\n",
    "                \"filename\": model_file,\n",
    "                \"format\": \"exodus\"\n",
    "            }            \n",
    "        }})\n",
    "    \n",
    "    # Provide information on the physical equation.\n",
    "    master_file.update({       \n",
    "        \"physics\": {\n",
    "            \"wave-equation\": {\n",
    "                \"time-stepping-scheme\": \"newmark\",\n",
    "                \"start-time-in-seconds\": -3e-3,\n",
    "                \"time-step-in-seconds\": 3e-6,\n",
    "                \"end-time-in-seconds\": 5e-3,\n",
    "                \"source-toml-filename\": str(results_folder / f\"{name}.toml\"),\n",
    "                \"boundaries\": [\n",
    "                    {\n",
    "                        \"type\": \"absorbing\",\n",
    "                        \"side-sets\": [\"x0\", \"x1\", \"y0\"]\n",
    "                    }\n",
    "                ]\n",
    "            },\n",
    "        }})\n",
    "    \n",
    "    # Provide information relating to point receivers.\n",
    "    master_file.update({\n",
    "        \"output\": {\n",
    "            \"point-data\": {\n",
    "                \"fields\": [\"u_ELASTIC\"],\n",
    "                \"sampling-interval-in-time-steps\": 1,\n",
    "                \"filename\": \"receivers.h5\",\n",
    "                \"format\": \"ASDF\",\n",
    "                \"receiver\": receivers\n",
    "            }\n",
    "        }})\n",
    "    \n",
    "    return master_file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate a place to store the output.\n",
    "results_folder = Path(\"observed_data\")\n",
    "os.makedirs(str(results_folder), exist_ok=True)\n",
    "true_mesh.write_exodus(str(results_folder / \"true_model.e\"))\n",
    "\n",
    "# Run our simulations one-by-one.\n",
    "input_files = {}\n",
    "for name, parameters in simulations.items():\n",
    "    \n",
    "    # Write the source file for this simulation.\n",
    "    with open(str(results_folder / f\"{name}.toml\"), \"w\") as fh:\n",
    "        toml.dump({\"source\": [parameters[\"source\"][0]]}, fh)\n",
    "        \n",
    "    # Master input file definition (extensive documentation online).\n",
    "    input_file = get_master_input(\"observed_data/true_model.e\")\n",
    "    \n",
    "    # Let's also add a movie to see what's going on.\n",
    "    input_file[\"output\"].update({\n",
    "        \"volume-data\": {\n",
    "            \"fields\": [\"u_ELASTIC\"],\n",
    "            \"sampling-interval-in-time-steps\": 10,\n",
    "            \"filename\": \"movie.h5\",\n",
    "            \"format\": \"HDF5\",\n",
    "            \"polynomial-order\": 4,\n",
    "            \"region-of-interest\": False}})\n",
    "    \n",
    "    input_files[name] = input_file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that everything is set up, we just need to fire off the simulation jobs. One options is to do this manually by writing the input files to disk and either running the jobs locally, or copying our inputs to a remote cluster and running everything there. With Salvus_Flow_ however, this whole process becomes much easier. Below we'll submit each job one-by-one to our local machine, wait for it to complete, and then get all of the output data with an API call. Note that if the job required a remote cluster to run (e.g. if it required a supercomputer), we could simply point the `site_name` argument of the `salvus_flow.run_salvus` function to the relevant cluster. In this case Salvus_Flow_ would copy everything to the remote machine, monitor the remote queue, wait for job completion, and then copy the output back to our local computer. From the user's perspective, nothing changes whether Salvus_Compute_ is run locally or remotely.\n",
    "\n",
    "Before we get started, let's just write a quick function to make it easy to visualize seismograms. We'll use this to plot the \"observed\" data from the true model, the synthetic data from the starting model, and eventually, the data after our inversion."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_seismograms(file, receivers, label, ax):\n",
    "    \"\"\"\n",
    "    A simple function which allows one to plot seismograms on top of each other.\n",
    "    :param file: ASDF file containing the seismograms.\n",
    "    :param receivers: Which receiver indices to plot.\n",
    "    :param label: What label to plot for this data.\n",
    "    :param ax: Pre-existing figure axes on which to overplot.\n",
    "    \"\"\"\n",
    "    with pyasdf.ASDFDataSet(file, mode=\"r\") as dataset:\n",
    "        for _i, rec in enumerate(receivers):            \n",
    "            data = dataset.waveforms[dataset.waveforms.list()[rec]]\n",
    "            ax[_i].plot(data.displacement[0].times() - 6e-4, data.displacement[0].data, label=label)\n",
    "            ax[_i].set_xlabel(\"Time (s)\")\n",
    "            ax[_i].set_ylabel(\"Displacement (m)\")\n",
    "            ax[_i].legend() "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now it's time to actually run the simulations. To visualize what we're doing, let's plot some of the data we compute along the way. Here I'm just choosing to plot data from 4 receivers coming from one shot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run simulations.\n",
    "f, ax = plt.subplots(4, 1, squeeze=True, figsize=(15, 15), sharex=True)\n",
    "for name, params in input_files.items():\n",
    "    \n",
    "    # Run simulations.\n",
    "    output_folder = str(results_folder / name)\n",
    "    salvus_flow.api.run(\n",
    "        ranks=4,\n",
    "        get_all=True,\n",
    "        overwrite=True,\n",
    "        site_name=\"alireza\",\n",
    "        input_file=params,\n",
    "        output_folder=output_folder\n",
    "    )\n",
    "    \n",
    "    # Visualize some seismograms.\n",
    "    if name == \"simulation_000\":\n",
    "        plot_seismograms(str(Path(output_folder) / \"receivers.h5\"), \n",
    "                         [0, 5, 10, 15], \"True Data\", ax)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The data looks alright -- now we have everything we need to start the inversion. Before we do this though, let's have a look at how our synthetic data looks through the starting model -- this is the model with the averaged material parameters. Below we just need to swap the true model for our starting model, and then save the seismograms somewhere else so we can visuzlize them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run simulations.\n",
    "f, ax = plt.subplots(4, 1, squeeze=True, figsize=(15, 15), sharex=True)\n",
    "for name, params in input_files.items():\n",
    "    \n",
    "    # Swap the mesh to the staring model.\n",
    "    params = get_master_input(\"starting_model.e\")\n",
    "    \n",
    "    # Run simulations.\n",
    "    output_starting = str(f\"starting_model/{name}\")\n",
    "    output_observed = str(f\"observed_data/{name}\")\n",
    "    salvus_flow.api.run(\n",
    "        ranks=4,\n",
    "        get_all=True,\n",
    "        overwrite=True,\n",
    "        site_name=\"alireza\",\n",
    "        input_file=params,\n",
    "        output_folder=output_starting\n",
    "    )\n",
    "    \n",
    "    # Visualize some seismograms.\n",
    "    if name == \"simulation_000\":\n",
    "        plot_seismograms(str(Path(output_starting) / \"receivers.h5\"), \n",
    "                         [0, 5, 10, 15], \"Starting model\", ax)\n",
    "        plot_seismograms(str(Path(output_observed) / \"receivers.h5\"), \n",
    "                         [0, 5, 10, 15], \"True model\", ax)\n",
    "\n",
    "f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As expected, the seismograms don't match up. Now, let's see how much better we can make this fit using FWI.\n",
    "\n",
    "## Full-waveform inversion\n",
    "\n",
    "While using Salvus_Flow_ to run forward seismic simulations is convienient, its real power is shown when running in workflow management mode for iterative FWI. In general, dealing with FWI workflows is a difficult problem. One needs to orchestrate forward and adjoint simulations runs on remote compute systems, interact with a nonlinear optimization library, and keep track of intermediate results, among other things. In fact, I'd venture that the complexity of realistic FWI workflows has been one of the cheif impediments to efficiently running inversions at scale. In the next few cells, we'll see what Salvus_Flow_ has to offer in this department.\n",
    "\n",
    "To set of our workflow, we need to specify several parameters. First off, we'll tell Salvus_Flow_ where to read the descriptions of our seismic sources. Each source defines a separate \"simulation\", with a possibly unique set of receivers. We also need to tell _Flow_ how to find the observed seismic data with which to generate misfits."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read in all the relevant source descriptions.\n",
    "sources = list(Path(\"observed_data\").glob(\"*.toml\"))\n",
    "\n",
    "# Tell Flow what the observed data files will be called.\n",
    "observed_data_template = (Path(\"observed_data\") / \"{source_name}\" / \"receivers.h5\")    \n",
    "observed_data_dict = {\n",
    "    i.stem: str(observed_data_template).format(source_name=i.stem)\n",
    "    for i in sources}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we'll set-up a couple of callback functions which Salvus_Flow_ will use to help us compute waveform misfits and adjoint sources. For a select collection of misfits _Flow_ provides some templates which are ready to go. For example, below we will use the simple $L2$ waveform difference to drive the inversion. To this function we need to pass instructions generated above on how to read the observed field data. You'll also notice the `use_celery` argument. When dealing with large, heterogeneous inversions, the ability to recover from failures is of great importance. Using the `Celery` task queue allows _Flow_ to spin up and keep track of all the different workflow branches using separate processes. This ensures that even if an individual element in the inversion fails, the entire workflow can be recovered from the point of failure. For small jobs `Celery` can be a bit of an overkill, so we'll turn it off here. We'll also pass the source and receiver objects we created above to a callback function "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# To try cross-correlation misfit.\n",
    "# misfit_type=\"CC_WITH_DATA\",\n",
    "# misfit_kwargs={\n",
    "#     \"max_cc_shift_in_seconds\": 1.0\n",
    "# },\n",
    "\n",
    "get_sources_and_receivers = get_sources_and_receivers_callback_function(\n",
    "    sources=sources, receivers={\"receiver\": receivers})\n",
    "\n",
    "use_celery = False\n",
    "get_misfits_and_adjoint_sources = \\\n",
    "    get_misfit_and_adjoint_source_callback_function(\n",
    "        observed_data_dict=observed_data_dict,\n",
    "        problem_dimensions=2,\n",
    "        misfit_type=\"L2\",\n",
    "        misfit_kwargs={\n",
    "        },\n",
    "        use_celery=use_celery)\n",
    "\n",
    "def get_callbacks():\n",
    "    return {\n",
    "        \"get_sources_and_receivers\":\n",
    "            get_sources_and_receivers,\n",
    "        \"compute_misfits_and_adjoint_sources\":\n",
    "            get_misfits_and_adjoint_sources\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we need to specify a bunch of settings that will determine the specifics of the inversion. There's several sections here -- let's look at them one by one. First, we need to specify some basic \"meta\" information about the whole workflow. This includes parameters like our parallel decomposition, walltimes for any associated queuing systems, and so-on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_meta_config(ranks=2):\n",
    "    \"\"\"\n",
    "    Return the 'meta' group for our FWI workflow.\n",
    "    :param ranks: Number of ranks to run the wave and heat-equations with.\n",
    "    \"\"\"\n",
    "    return {\n",
    "        \"ranks_per_simulation\": 4,\n",
    "        \"ranks_for_heat_equation\": 4,\n",
    "        \"submit_all_simulations_at_once\": False,\n",
    "        \"walltime_forward_waveform_simulation_in_seconds\": 60,\n",
    "        \"walltime_adjoint_waveform_simulation_in_seconds\": 60,\n",
    "        \"walltime_heat_equation_simulation_in_seconds\": 60,\n",
    "        \"max_number_of_retries\": 2\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we need to set up the input files for Salvus_Opt_. We will choose a trust-region l-BFGS approach, and must also therefore choose the \"max_history\", or how many previous gradients will be used in the l-BFGS approximation of the inverse Hessian. If this is set to 0, we will simply be working with a steepest descent method. We also need to give the optimization routines some idea of the initial and maximum relative perturbations for our parameter updates. It helps to use some physical intuition here. We know that our final model needs to be relatively close to the initial model, and we should not consider model updates which are very large. We then also need to set a series of criteria which, if satisfied, will signal that the problem has convered. In this example, we will assume convergence if either the misfit value at a given iteration are less than 10% of the initial value, or the norm of the gradient is less than 0.1% of its initial value. Finally, we also need tell Salvus_Opt_ where to find our initial model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_salvusopt_config(tol_misfit_relative=0.01):\n",
    "    \"\"\"\n",
    "    Return the 'salvus_opt' group for our FWI workflow.\n",
    "    :param tol_misfit_relative: Consider the problem converged at this relative reduction in misfit.\n",
    "    \"\"\"\n",
    "    return {\n",
    "        \"method\": {\n",
    "            # Set the inversion method.\n",
    "            \"type\": \"trust-region\",\n",
    "            \"lbfgs_max_history\": 5,\n",
    "            # Set the size of model perturbations.\n",
    "            \"max_relative_model_perturbation\": 0.1,\n",
    "            \"initial_max_relative_model_perturbation\": 0.05,\n",
    "            # When can we assume convergence?\n",
    "            \"convergence-criteria\": {\n",
    "                \"tol-misfit-relative\": 0.01,\n",
    "                \"tol-misfit-absolute\": 0.0,\n",
    "                \"tol-norm-gradient-relative\": 0.001,\n",
    "                \"tol-norm-gradient-absolute\": 0.0,\n",
    "                \"max-number-of-iterations\": 10,\n",
    "                \"tol-model-update-relative\": 0.0001\n",
    "            }},\n",
    "        # What is our starting model, and what fields to we want to invert for?\n",
    "        \"model\": {\n",
    "            \"initial-model\": Path(\"starting_model.e\"),\n",
    "            \"fields\": [\"VP\", \"VS\", \"RHO\"],\n",
    "            \"type\": \"exodus\"\n",
    "        }\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also need to specify a minimal set of parameters which describe the forward and adjoint wave simulations, which Salvus_Flow_ will pass on the Salvus_Compute_. The sources and receivers themselves do not need to be specified here -- we'll handle these later on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_wave_propagation_config():\n",
    "    \"\"\"\n",
    "    Return the 'wave_propagation_settings' group for our FWI workflow.\n",
    "    \"\"\"\n",
    "    return {\n",
    "        \"physics\": {\n",
    "            \"wave-equation\": {\n",
    "                \"start-time-in-seconds\": -3e-3,\n",
    "                \"time-step-in-seconds\": 3e-6,\n",
    "                \"end-time-in-seconds\": 5e-3,\n",
    "                \"boundaries\": [\n",
    "                    {\n",
    "                        \"type\": \"absorbing\",\n",
    "                        \"side-sets\": [\"x0\", \"x1\", \"y0\"]\n",
    "                    }\n",
    "                ]\n",
    "            }\n",
    "        },\n",
    "        \"domain\": {\n",
    "            \"dimension\": 2,\n",
    "            \"polynomial-order\": 4,\n",
    "        },\n",
    "         \"output\": {\n",
    "            \"point-data\": {\n",
    "                \"fields\": [\"u_ELASTIC\"],\n",
    "                \"sampling-interval-in-time-steps\": 1\n",
    "            },\n",
    "            \"volume-data\": {\n",
    "                \"sampling-interval-in-time-steps\": 10,\n",
    "                \"polynomial-order\": 4\n",
    "            }\n",
    "        }\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the forward simulations described, we now need to tell Salvus how to run the heat-equation based smoothing. If we normalize the run-time of the heat-equation to 1.0, then the smoothing lengths correspond to the variances in meters in each spatial dimension. The smoothing operation helps us regularize the inverse problem, and is one way of using our physical intuition to mitigate the under-determined nature of FWI. You can of course change these lengths to see their effect on the final results, in addition to making them anisotropic, or even spatially variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_heat_equation_config():\n",
    "    return {\n",
    "        \"physics\":  {\n",
    "            \"heat-equation\": {\n",
    "                \"start-time-in-seconds\": 0.0,\n",
    "                \"end-time-in-seconds\": 1.0,\n",
    "                \"time-step-in-seconds\": 0.001,\n",
    "                \"smoothing-lengths\": [0.05, 0.05] # Variance of smoothing operator (m)\n",
    "            }\n",
    "        }\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now with everything set up, we can go ahead and initialize our FWI workflow. If the workflow is run in a Jupyter notebook, a small widget will pop up that will allow you to monitor the ongoing inversion. Behind the scenes, Salvus_Opt_ and Salvus_Flow_ are working together to run the show, dispatching tasks to Salvus_Compute_ as needed. In this particular example, the inversion will run until the convergence criteria specified earlier are satisfied. As the iterations proceed, you'll see a graph visualizing the behaviour of the misfit function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Construct our master input file.\n",
    "workflow_config = {\n",
    "    \"meta\": get_meta_config(),\n",
    "    \"salvus_opt_settings\": get_salvusopt_config(),\n",
    "    \"wave_propagation_settings\": get_wave_propagation_config(),\n",
    "    \"heat_equation_settings\": get_heat_equation_config(),\n",
    "    \"callbacks\": get_callbacks()\n",
    "}\n",
    "\n",
    "# Remove the output directory if it already exists.\n",
    "if os.path.exists(\"workflow_run_directory\"):\n",
    "    shutil.rmtree(\"workflow_run_directory\")\n",
    "    \n",
    "# Run our FWI workflow!!\n",
    "w = Workflow(base_dir=\"workflow_run_directory\",\n",
    "             workflow_type=\"FWI\",\n",
    "             salvus_flow_site_name=\"alireza\",\n",
    "             config=workflow_config, \n",
    "             use_celery=use_celery)\n",
    "w"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that the problem has converged, let's check out the results. First, let's take a look at the final model. Below we read the final model into an `unstructured_mesh` object so we can visualize it in the notebook, but you could of course just open the file in `Paraview` or `VisIt` instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot final model.\n",
    "final_model = salvus_mesh.unstructured_mesh.UnstructuredMesh.from_exodus(\n",
    "    \"./workflow_run_directory/__RESULTS/final_model.e\")\n",
    "final_model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, before we finish with FWI for now, let's take a look at the resultant synthetic seismograms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f, ax = plt.subplots(4, 1, squeeze=True, figsize=(15, 15), sharex=True)\n",
    "\n",
    "final_seismos = \"./workflow_run_directory/__RESULTS/simulation_000/receivers.h5\"\n",
    "plot_seismograms(final_seismos, [0, 5, 10, 15], \"Final model\", ax)\n",
    "plot_seismograms(\"observed_data/simulation_000/receivers.h5\", [0, 5, 10, 15], \"True model\", ax)\n",
    "plot_seismograms(\"starting_model/simulation_000/receivers.h5\", [0, 5, 10, 15], \"Starting model\", ax)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
