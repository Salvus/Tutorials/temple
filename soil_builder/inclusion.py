from abc import ABC, abstractmethod
from typing import Tuple, Dict, Callable, Optional

import numpy as np
from scipy.optimize import fsolve


class Inclusion(ABC):
    """
    A base class to handle soil -- or other -- inclusions.
    """

    def __init__(self, parameters: Optional[Dict[str, float]] = None) -> None:
        self._p = parameters

    def get_parameter(self, par: str):
        """
        Get a parameter value for this inclusion.
        :param par: Parameter name
        :return: Parameter value.
        """
        return self._p[par]

    @abstractmethod
    def is_inside(self, x):
        pass


class Triangle(Inclusion):
    """
    A class encapsulating an inclusion defined by a triangle with a liner
    shape mapping.
    """

    def __init__(self, x: np.ndarray,
                 parameters: Optional[Dict[str, float]] = None) -> None:
        """
        Construct a triangle by specifying the corner points in a list of
        coordiante pairs.
        :param x: Matrix of vertices with dimension (3, 2).
        :param parameters: Dictionary of parameter values {name, value}.
        """
        super().__init__(parameters)

        if x.shape != (3, 2):
            raise ValueError("Must provide 3 coordinate pairs for a triangle.")

        # Ensure we don't have a crazy shape.
        self._x = x
        self._t = np.array([
            [x[0, 0] - x[2, 0], x[1, 0] - x[2, 0]],
            [x[0, 1] - x[2, 1], x[1, 1] - x[2, 1]]])
        if np.linalg.det(self._t) <= 0:
            raise ValueError("Malformed triangle detected.")

    def is_inside(self, x):
        """
        Perform a Barycentric coordinate transform to determine whether or
        not a trial point x is within the inclusion.
        :param x: Trial point.
        :return: Reference coordinates.
        """
        l1, l2 = np.linalg.inv(self._t).dot(x - self._x[2, :])
        return -1 + 2 * np.array([l1, l2, 1 - l1 - l2], dtype=float)


class Quadrilateral(Inclusion):
    """
    A class encapsulating an inclusion defined by a quadrilateral. Shape of
    the quadrilateral is defined by first order Lagrange polynomials.
    """

    def __init__(self, x: np.ndarray,
                 parameters: Optional[Dict[str, float]] = None) -> None:
        """
        Construct a rectangle by specifying the corner points in a list of
        coordinate pairs.
        :param x: Matrix of vertices with dimension (4, 2).
        :param parameters: Dictionary of parameter values {name, value}.
        """
        super().__init__(parameters)

        if x.shape != (4, 2):
            raise ValueError("Must provide 4 coordinate pairs for a "
                             "rectangle.")

        # Ensure we don't have a crazy shape.
        self._x = x
        test_x = np.array([0, 0], dtype=float)
        if np.linalg.det(self._compute_jacobian(test_x)) <= 0:
            raise ValueError("Object you have defined is not a rectangle. A "
                             "rectangle needs to be defined with 4 vertices "
                             "ordered clockwise.")

    @staticmethod
    def _lagrange(r: float, s: float) -> np.ndarray:
        """
        Evaluate the lagrange polynomials at a point in reference coordinates.
        :param r: R-coordinate (reference space).
        :param s: S-coordinate (reference space).
        :return: Lagrange polynomials evaluated as (r, s).
        """
        return np.array([
            0.25 * (1 - r) * (1 - s),
            0.25 * (1 + r) * (1 - s),
            0.25 * (1 + r) * (1 + s),
            0.25 * (1 - r) * (1 + s)
        ])

    @staticmethod
    def _derivative_lagrange(r: float, s: float) -> np.ndarray:
        """
        Get the derivatives of the tensorized Lagrange polynomials at a point
        in reference space.
        :param r: R-coordinate (reference space).
        :param s: S-coordinate (reference space).
        :return: Derivatives in (r, s).
        """

        nodes_r = np.array([-1, +1, +1, -1], dtype=float)
        nodes_s = np.array([-1, -1, +1, +1], dtype=float)
        return np.array(
            [[0.25 * nodes_r[0] * (s * nodes_s[0] + 1),
              0.25 * nodes_r[1] * (s * nodes_s[1] + 1),
              0.25 * nodes_r[2] * (s * nodes_s[2] + 1),
              0.25 * nodes_r[3] * (s * nodes_s[3] + 1)],
             [0.25 * nodes_s[0] * (r * nodes_r[0] + 1),
              0.25 * nodes_s[1] * (r * nodes_r[1] + 1),
              0.25 * nodes_s[2] * (r * nodes_r[2] + 1),
              0.25 * nodes_s[3] * (r * nodes_r[3] + 1)]])

    def _compute_jacobian(self, x: np.ndarray):
        """
        Compute the Jacobian of the coordinate transform.
        :param x:  Coordinate in reference points.
        :return: Jacobian at point x.
        """
        return self._derivative_lagrange(x[0], x[1]).dot(self._x)

    def _compute_inv_jacobian(self, x: np.ndarray, *args):
        """
        Compute the inverse Jacobian of the coordinate transform, which is the
        Jacobian for the inverse coordinate transform.
        :param x:  Coordinate in reference points.
        :param args: Dummy args to make Scipy happy.
        :return: Inverse Jacobian at point x.
        """
        return np.linalg.inv(
            self._derivative_lagrange(x[0], x[1]).dot(self._x))

    def is_inside(self, x: np.ndarray) -> np.ndarray:
        """
        Use Newton's method to determine whether or not a given trial point 
        in physical coordiantes (x) is inside the quadrilateral. 
        :param x: Trial point.
        :return: Reference coordinates.
        """

        def func(x_trial_ref: np.ndarray, x_target: np.ndarray,
                 vtx: np.ndarray, lagrange: Callable) -> np.ndarray:
            """
            Compute the residuals of the current coordinate estimate.
            :param x_trial_ref: Current estimate in reference coordiantes.
            :param x_target: Target value in physical coordinates.
            :param vtx: Vertices of the quadrilateral.
            :param lagrange: Lagrange polynomial to map ref -> physical.
            :return: Objective value.
            """
            x_trial_inside = lagrange(*x_trial_ref).dot(vtx)
            return x_trial_inside - x_target

        # Shortcut to bail out.
        max_coeff = np.max(self._x, axis=0)
        min_coeff = np.min(self._x, axis=0)
        if ((x - min_coeff) < 0).any() or ((x - max_coeff) > 0).any():
            return np.array([1.1, 1.1])

        x_trial = np.array([0, 0], dtype=float)
        return fsolve(func, x_trial, (x, self._x, self._lagrange),
                      self._compute_inv_jacobian, full_output=False)
