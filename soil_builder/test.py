import pytest

import numpy as np
from numpy.testing import assert_allclose

from inclusion import Quadrilateral, Triangle


def get_trivial_tri_vtx():
    return np.array([
        [-1.0, -1.0],
        [+1.0, -1.0],
        [0.0, +1.0]])


def test_init_tri():
    with pytest.raises(ValueError):
        Triangle(np.array([
            [-1.0, -1.0],
            [+1.0, -1.0],
            [-1.0, -1.0]]))


def test_is_inside_tri():
    t = Triangle(get_trivial_tri_vtx())
    c = t.is_inside(np.array([0, 0], dtype=float))
    assert_allclose(c, np.array([-0.5, -0.5, 0.]))

    t = Triangle(get_trivial_tri_vtx())
    c = t.is_inside(np.array([0, +1], dtype=float))
    assert_allclose(c, np.array([-1., -1., 1.]))

    t = Triangle(get_trivial_tri_vtx())
    c = t.is_inside(np.array([0, -1.1], dtype=float))
    assert_allclose(c, np.array([0.05, 0.05, -1.1]))


def get_trivial_vtxs():
    return np.array([
        [-1.0, -1.0],
        [+1.0, -1.0],
        [+1.0, +1.0],
        [-1.0, +1.0]])


def test_init_rectangle():
    with pytest.raises(ValueError):
        Quadrilateral(np.array([1.0, 1.0]))


def test_lagrange():
    # Values are correct.
    assert_allclose(
        Quadrilateral._lagrange(0.0, 0.0),
        np.array([0.25, 0.25, 0.25, 0.25]))

    # Interpolation is correct.
    assert_allclose(
        Quadrilateral._lagrange(0.0, 0.0).dot(
            np.array([
                [0.0, 0.0],
                [1.0, 0.0],
                [1.0, 1.0],
                [0.0, 1.0]])), np.array([0.5, 0.5]))


def test_derivatives_lagrange():
    assert_allclose(
        Quadrilateral._derivative_lagrange(0, 0),
        np.array([[-0.25, 0.25, 0.25, -0.25],
                  [-0.25, -0.25, 0.25, 0.25]]))


def test_jacobian():
    # Jacobian for this element should be the identity matrix.
    r = Quadrilateral(get_trivial_vtxs())
    assert_allclose(r._compute_jacobian([0.0, 0.0]), np.eye(2, 2))

    # Don't accept crazy quad.
    vtx_crazy = get_trivial_vtxs()
    vtx_crazy[0, :] = [+2, +2]
    with pytest.raises(ValueError):
        Quadrilateral(vtx_crazy)


def test_is_inside():
    r = Quadrilateral(get_trivial_vtxs())
    c = r.is_inside(np.array([0, 0], dtype=float))
    assert_allclose(c, np.array([0, 0], dtype=float))

    r = Quadrilateral(get_trivial_vtxs())
    c = r.is_inside(np.array([1, 1], dtype=float))
    assert_allclose(c, np.array([1, 1], dtype=float))

    r = Quadrilateral(get_trivial_vtxs() * 2)
    c = r.is_inside(np.array([-1, 1], dtype=float) * 2)
    assert_allclose(c, np.array([-1, 1], dtype=float))

    # Not in convex hull.
    r = Quadrilateral(get_trivial_vtxs())
    c = r.is_inside(np.array([-1.1, 1], dtype=float))
    assert_allclose(c, np.array([1.1, 1.1], dtype=float))
