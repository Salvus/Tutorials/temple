from typing import Dict, Tuple

import matplotlib.pyplot as plt
import numpy as np
import salvus_mesh
from matplotlib.collections import PatchCollection
from matplotlib.patches import Polygon
from salvus_mesh import Skeleton
from scipy.interpolate import RegularGridInterpolator
from scipy.ndimage import gaussian_filter

from salvus_mesh.unstructured_mesh import UnstructuredMesh
from .inclusion import Inclusion


class Domain:
    """
    A simple class to wrap a computational domain.
    """

    def __init__(self, max_x: float, max_y: float, p: Dict[str, float],
                 min_x: float = 0, min_y: float = 0) -> None:
        """
        Set up a simple rectangular domain in 2 dimensions.
        :param max_x: Maximum x-value.
        :param max_y: Maximum y-value.
        :param min_x: Minimum x-value.
        :param min_y: Minimum y-value.
        :param p: Parameters to attach to the background.
        """

        self._min_x = min_x
        self._max_x = max_x
        self._min_y = min_y
        self._max_y = max_y

        self._p = p
        self._inclusions = []
        self._constraints = {}

        assert set(p.keys()) == {"VP", "VS", "RHO"}

    def attach_inclusion(self, inclusion: Inclusion) -> None:
        """
        Attach some inclusion to this domain.
        :param inclusion: Inclusion object.
        """
        self._inclusions.append(inclusion)

    def mesh(self, max_frequency: float, min_velocity: str = "inclusion",
             elm_per_wavelength: int = 2, eps: float = 1e-3,
             smoothing_length: float = 0.1, num_absorbing_layers: int = 7)\
            -> UnstructuredMesh:
        """
        Generate a Salvus mesh from the background domain and any associated
        inclusions.
        :param max_frequency: Mesh will accurate to this frequency.
        :param min_velocity: Minimum velocity in the mesh. Should be one of
            - 'inclusion': Detect slowest velocity from inclusion VS values.
            - 'background': Use background VS and refine the mesh
                surrounding each inclusion.
            - float: Specify a velocity to mesh for.
        :param elm_per_wavelength: Elements per wavelength. Will control the
            accuracy of the simulations. For high accuracy, we usually choose
            2 elements per maximum frequency.
        :param eps: Tolerance for inclusion detection. Default value should
            be fine, but change this if you are having issues.
        :param smoothing_length: The velocity model needs to be smoothed
            somewhat before it is attached to the mesh. This is the standard
            deviation of a Gaussian smoothing kernel in meters.
        :param num_absorbing_layers: Number of damping layers to add.
        :return: Salvus mesh ready fro writing and simulation.
        """

        # Get the minimum velocity dependent on options.
        if type(min_velocity) == str:

            vel_params = ["inclusion", "background"]
            if min_velocity not in vel_params:
                raise ValueError(
                    "Min velocity must be one of [ " + ", ".join(vel_params)
                    + " ]")

            if min_velocity == "inclusion" and len(self._inclusions) == 0:
                raise ValueError(
                    "No inclusions detected! Can not use inclusions "
                    "velocities for meshing.")

            # Figure out the minimum velocity.
            min_velocity_val = np.infty
            if min_velocity == "inclusion":
                for inclusion in self._inclusions:
                    min_velocity_val = min(
                        inclusion.get_parameter("VS"), min_velocity_val)
            elif min_velocity == "background":
                min_velocity_val = self._p["VS"]

        else:
            min_velocity_val = min_velocity

        # Accuracy of the mesh.
        min_wavelength = min_velocity_val / max_frequency
        max_edge_length = min_wavelength / elm_per_wavelength

        # Expand for absorbing boundaries.
        min_x = self._min_x - num_absorbing_layers * max_edge_length
        max_x = self._max_x + num_absorbing_layers * max_edge_length
        min_y = self._min_y - num_absorbing_layers * max_edge_length
        max_y = self._max_y + num_absorbing_layers * max_edge_length
        nelem_x = np.ceil((max_x - min_x) / max_edge_length).astype(int)
        nelem_y = np.ceil((max_y - min_y) / max_edge_length).astype(int)

        # Generate base mesh object.
        mesh = Skeleton([salvus_mesh.StructuredGrid2D.rectangle(
            min_x=min_x, max_x=max_x,
            min_y=min_y, max_y=max_y,
            nelem_x=nelem_x, nelem_y=nelem_y)]).get_unstructured_mesh()

        # Maybe refine if we want to.
        if min_velocity == "background":
            for i in range(1):
                c = mesh.get_element_centroid()
                ref_mask = np.zeros(mesh.nelem, dtype=np.bool)
                for incl in self._inclusions:
                    ref_c = np.apply_along_axis(incl.is_inside, axis=1, arr=c)
                    ref_mask += np.all(np.abs(ref_c) <= 1. + eps, axis=1)
                mesh.refine_locally(ref_mask, refinement_style="stable")

        # Generate a grid on which to interpolate.
        nix = nelem_x * 2
        niy = nelem_y * 2
        x, y = np.mgrid[min_x:max_x:complex(nix), min_y:max_y:complex(niy)]
        pos = np.stack([x.ravel(), y.ravel()])

        # Attach the background velocities.
        p_grids = {}
        for k, v in self._p.items():
            p_grids[k] = np.ones(pos.shape[1]) * v

        # Find all the inclusions, and update their velocities.
        for incl in self._inclusions:
            test = np.apply_along_axis(incl.is_inside, axis=0, arr=pos)
            for k, v in incl._p.items():
                mask = np.bool(pos.shape[1]) * np.all(
                    np.abs(test) <= 1. + eps, axis=0)
                p_grids[k][np.where(mask == 1)] = v

        # Interpolate the smoothed velocity field onto the mesh.
        interpolators = {}
        grid_x = np.linspace(min_x, max_x, nix)
        grid_y = np.linspace(min_y, max_y, niy)
        for k, v in p_grids.items():
            dx = grid_x[1] - grid_x[0]
            interpolators[k] = RegularGridInterpolator(
                (grid_x, grid_y),
                gaussian_filter(v.reshape(len(x), len(y)),
                                smoothing_length / dx,
                                mode="nearest", cval=self._p[k]),
                bounds_error=False, fill_value=None)

        # Attach fields to mesh.
        mesh.attach_field("fluid", np.zeros(mesh.nelem))
        for k, v in interpolators.items():
            mesh.attach_field(k, v(mesh.points))

        # Attach bound constraints if they are present.
        for k1, v1 in self._constraints.items():
            for k2, v2 in v1.items():
                prefix = "LB_" if k1 == "lower" else "UB_"
                mesh.attach_field(prefix + k2, np.ones(mesh.npoint) * v2)

        mesh.find_side_sets()
        return mesh

    def add_constraints(self, bound: str, value: Dict[str, float]) -> None:
        """
        Add lower and upper bound constraints to the model. During the
        inversion, the parameters will not be allowed to exceed the bounds
        specified in the constraints.
        :param bound: Which bound is this? Choice are "upper" and "lower".
        :param value: Value of the constraint for a parameter.
        """

        allowed_constraints = {"upper", "lower"}
        if bound not in allowed_constraints:
            raise ValueError("'bound' must be one of " + ", ".join(
                allowed_constraints))

        if set(value.keys()) != set(self._p.keys()):
            raise ValueError("Must specify constraints for all parameters.")

        self._constraints[bound] = value

    def plot(self) -> Tuple[plt.figure, plt.axis]:
        """
        Simple plot of all the inclusions.
        :return: Figure and axes.
        """
        f, ax = plt.subplots(figsize=(10, 10))

        patches = []
        for shape in self._inclusions:
            patches.append(Polygon(shape._x, closed=True))

        ax.add_collection(PatchCollection(patches))

        ax.set_xlim(self._min_x, self._max_x)
        ax.set_ylim(self._min_y, self._max_y)
        ax.set_xlabel("Distance (m)")
        ax.set_ylabel("Distance (m)")
        ax.set_facecolor('lightgrey')

        return f, ax
